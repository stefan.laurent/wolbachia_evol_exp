Description of the experiment
=============================

Original aim of the experiment: Find the selection coefficient of an allele.


## Background

Wolbachia pipientis is a bacteria species that lives in Drosophila. In
experimental work, one locus has been identified as a candidate to be
adaptational to different temperature environments.

## Aim

Provide the Likelihood of observed trajectories given selection.

## Experimental setup

This simulator provides trajectories of a given experimental setup. We simulate
explicit reproduction under the experimental setup.
An individual bears either allele 1 or 2. Here, an individual is defined as the
unit that bears the actual allele. However, in reality, one Drosophila
individual bears the bacteria that bear the allele. The starting frequency is
given as freq_init = 0.5 * 400 Drosophila eggs. They grow out and some of them
die. The survivors produce a given amount of eggs. From all the eggs again 400
are sampled. Each generation the eggs are genotyped to their Wolbachia allele.


We model the Wolbachia similar to mitochondria. We inherit the
allele only through the female, if it comes from the male, it will not be
inherited.

