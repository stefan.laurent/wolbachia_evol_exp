import numpy as np
import pandas as pd
import datetime
import sys

# log, create log file
with open(snakemake.log.log1, "w", encoding="utf-8") as logfile:
    print(datetime.datetime.now(), end="\t", file=logfile)
    print(
        "start aggregating",
        file=logfile,
    )


filenames = snakemake.input.traj
traj_list = [pd.read_csv(infile, sep="\t", header=0) for infile in filenames]
result_table = pd.concat(traj_list, ignore_index=True)
result_table.to_csv(snakemake.output.traj)

with open(snakemake.log.log1, "a", encoding="utf-8") as logfile:
    print(datetime.datetime.now(), end="\t", file=logfile)
    print(f"saved dataframe to {snakemake.output.traj}", file=logfile)
