"""
return the randint from a file given the row id
"""


import sys
import numpy as np


infile, dim1, dim2 =  sys.argv[1:]
dim1, dim2 = int(float(dim1)), int(float(dim2))


seed = np.load(infile)[dim1][dim2]
print(seed, end="")
