"""
module to simulate the population
"""


localrules:
    create_randints,


rule create_randints:
    output:
        npy=protected("resources/rand.int.npy"),
    log:
        log1="logs/module01/create_randints.log",
    # conda: "../../config/env.yml"
    params:
        seed=int(float(config["seed"]["sim_seed"])),
        nseed=(
            int(float(config["seed"]["sim_seed_dim"])),
            int(float(config["seed"]["sim_seed_dim2"])),
        ),
    script:
        "../scripts/01.randints.py"


rule simulate_slim:
    output:
        traj=temp("results/trajectories/sim_{id}.s_{selcoeffid}.txt"),
    input:
        npy=rules.create_randints.output.npy,
        rand="workflow/scripts/01.get_seed.py",
        slim="workflow/scripts/01.wolbachia_experiment.slim",
    log:
        log1="logs/module01/simulate_slim/sim_{id}.s_{selcoeffid}.log",
    # conda: "../../config/env.yml"
    group:
        "slim_sim"
    threads: 1
    # resources:
    params:
        population_size=int(float(config["model_params"]["population_size"])),
        death_rate=float(config["model_params"]["death_rate"]),
        selection_coefficient=lambda wildcards: config["model_params"][
            "selection_coefficient"
        ][int(float(wildcards.selcoeffid))],
        generations_to_simulate=int(
            float(config["model_params"]["generations_to_simulate"])
        ),
        initial_allele_proportion=float(
            config["model_params"]["initial_allele_proportion"]
        ),
    shell:
        r"""
        seed=`python {input.rand} {input.npy} {wildcards.id} {wildcards.selcoeffid}`  # read seed
        echo "seed: ""$seed" > {log}  # read seed

        slim \
            -d seed=$seed \
            -d population_size={params.population_size} \
            -d death_rate={params.death_rate} \
            -d selection_coefficient={params.selection_coefficient} \
            -d generations_to_simulate={params.generations_to_simulate} \
            -d initial_allele_proportion={params.initial_allele_proportion} \
            -d output_file=\'{output.traj}\' \
            {input.slim} | tee > {log.log1}
        """


rule aggregate_slims:
    output:
        traj="results/trajectories.csv",
    input:
        traj=expand(
            rules.simulate_slim.output.traj,
            id=range(config["num_sims_per_condition"]),
            selcoeffid=range(len(config["model_params"]["selection_coefficient"])),
        ),
    log:
        log1="logs/module01/aggregate_slims.log",
    #conda: "../../config/env.yml"
    threads: 1
    # resources:
    script:
        "../scripts/01.aggregate_slims.py"


rule analyse_and_visualize:
    output:
        pdf="results/figures/trajectories.pdf",
    input:
        traj=rules.aggregate_slims.output.traj,
    log:
        log1="logs/module01/analyse_and_visualize.log"
    conda:
        "../../config/env.yml"
    script:
        "../scripts/01.analyse_and_visualize.R"
