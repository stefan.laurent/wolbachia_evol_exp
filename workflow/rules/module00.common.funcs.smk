"""
The helper module of the workflow

Here, we create the target file list and print them to the standard error
stream. So, we still maintain creating the dag/rulegraph by piping to dot
correct.

Additionally, we intend this module to contain all helper functions which are
needed for the workflow in order to maintain readability in the parent
Snakefile.
"""

import sys
import warnings


def target_files(wildcards, verbose=False):
    """
    function to provide the target file as a list dependent on the wildcards
    """
    target_file_list = []

    
    # This forces rerunning simulations because I treat sim result tables as temp
    if False:
        # simulation files
        target_file_list.extend(
            expand(
                rules.simulate_slim.output.traj,
                id=range(config["num_sims_per_condition"]),
                selcoeffid=range(len(config["model_params"]["selection_coefficient"])),
            )
        )


    # final figure
    target_file_list.append("results/figures/trajectories.pdf")


    # print the requested files to the standard error stream
    if verbose:
        target_file_list.sort(key=str.lower)
        print("\n" + "_" * 80, file=sys.stderr)
        for file_index, target_file in enumerate(target_file_list, start=1):
            print(f"  {file_index}.) {target_file}", file=sys.stderr)
        print("\n" + "=" * 80 + "\n" * 2, file=sys.stderr)

    return target_file_list


def check_configuration_file(config):
    """
    check a few necessities on some values provided in the config.yaml
    """
    return True
