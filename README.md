# Wolbachia

Model evolutionary experiment to investigate the selective impact of Wolbachia alleles to temperature environment


## How to run

 + Make sure all dependencies are provided, see config/env.yml
 + Knowledge of snakemake is useful
 + $ snakemake -j 1
 + if needed $ snakemake -j 1 --use-conda
 + -j n; with n providing the number of threads


## Todo

 + I turned off the conda environment for the simulations, it takes too long and doesn't make sense for the short simulations
 + Provide a useful analysis and plotting script (somewhat hardcoded at the moment)


## Open questions:

+ Should the death rate per generation be dependent on the Wolbachia allele?
+ Is transmitting only through mother correct?
+ Is it necessary to control for sex ratio, I assume 0.5 (most likely not important for the experiment). Shall the sex determination during generation be stochastic?


# Note

 + The slim script can be used to run an ABC as provided in the SLiM manual.
